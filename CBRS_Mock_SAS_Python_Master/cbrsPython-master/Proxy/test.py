import json
import time

import sas
from util import *

foo = sas.SasProxy()

device_a = json_load("device_a.json")

response = foo.Registration([device_a])
cbsdId = response["registrationResponse"][0]["cbsdId"]
print cbsdId

spectrum = {
    "cbsdId" : cbsdId,
    "inquiredSpectrum" : [
        {
            "lowFrequency"  : 3550000000,
            "highFrequency" : 3700000000,
        }
    ]
}
response = foo.SpectrumInquiry([spectrum])
#print(response)

channel = response["spectrumInquiryResponse"][0]["availableChannel"]
range   = channel[0]["frequencyRange"]
print(range)

grant = {
    "cbsdId" : cbsdId,
    "operationParam": {
        "maxEirp": 10,
        "operationFrequencyRange": {
            "lowFrequency" : range["lowFrequency"],
            "highFrequency": range["highFrequency"],
        }
    }
}
response = foo.Grant([grant]);
#print(response)

grantId    = response["grantResponse"][0]["grantId"]
hbInterval = response["grantResponse"][0]["heartbeatInterval"]
expireTime = localTime(response["grantResponse"][0]["grantExpireTime"])
print(grantId)
print(hbInterval)
print(expireTime)

heartbeat = {
    "cbsdId"         : cbsdId,
    "grantId"        : grantId,
    "grantRenew"     : False,
    "operationState" : "GRANTED"
}

response = foo.Heartbeat([heartbeat]);
#print(response)

stamp = localTime(response["heartbeatResponse"][0]["transmitExpireTime"])
print(stamp)

time.sleep(5)

heartbeat["operationState"] = "AUTHORIZED"
response = foo.Heartbeat([heartbeat]);
#print(response)

stamp = localTime(response["heartbeatResponse"][0]["transmitExpireTime"])
print(stamp)

relinquishment = {
    "cbsdId"         : cbsdId,
    "grantId"        : grantId
}

response = foo.Relinquishment([relinquishment]);
print(response)

