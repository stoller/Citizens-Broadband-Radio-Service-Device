#    Copyright 2018 SAS Project Authors. All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
"""Implementation of SasInterface."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
from six.moves import configparser
from request_handler import TlsConfig, RequestPost, RequestGet

CERTDIR   = "/users/stoller/certificates/"
PROXYCERT = CERTDIR + "UUTDomainProxy_unified.pem"
PROXYKEY  = CERTDIR + "UUTDomainProxyprivkey.key"
PROXYCA   = CERTDIR + "cbrs_ca1.pem"
SASURL    = "https://localhost:5000/v1.2"

class SasProxy(object):
    """Implementation of SAS client/proxy Interface."""

    def __init__(self):
        self.ca_cert     = PROXYCA
        self.client_cert = PROXYCERT
        self.client_key  = PROXYKEY
        self.sasurl      = SASURL
        self.tls_config  = TlsConfig(self.ca_cert,
                                     self.client_cert, self.client_key)
        pass
  
    def Registration(self, devices):
        request = {'registrationRequest': devices}
        return self._Request('registration', request)

    def SpectrumInquiry(self, spectrum):
        request = {'spectrumInquiryRequest': spectrum}
        return self._Request('spectrumInquiry', request)

    def Grant(self, grants):
        request = {'grantRequest': grants}
        return self._Request('grant', request)

    def Heartbeat(self, beats):
        request = {'heartbeatRequest': beats}
        return self._Request('heartbeat', request)

    def Relinquishment(self, grants):
        request = {'relinquishmentRequest': grants}
        return self._Request('relinquishment', request)

    def _Request(self, method_name, request):
        return RequestPost(self.sasurl + "/" + method_name,
                           request, self.tls_config)

    pass

