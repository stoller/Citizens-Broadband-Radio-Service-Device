#    Copyright 2018 SAS Project Authors. All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
"""Helper functions for test harness."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from datetime import datetime
from functools import wraps
import inspect
import json
import logging
import os
import random
import sys
import time
import uuid
import datetime
import pytz
import tzlocal
local_tz = tzlocal.get_localzone()

from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from jsonschema import validate, Draft4Validator, RefResolver
import jwt
from OpenSSL.crypto import load_certificate, FILETYPE_PEM

def json_load(fname):
  with open(fname) as fd:
    return json.load(fd)

def loadConfig(config_filename):
  """Loads a configuration file."""
  with open(config_filename, 'rb') as f:
    return json.loads(f.read())

def writeConfig(config_filename, config):
  """Writes a configuration file."""
  dir_name = os.path.dirname(config_filename)
  if not os.path.exists(dir_name):
    os.makedirs(dir_name)

  with open(config_filename, 'w') as f:
    f.write(
        json.dumps(config, indent=2, sort_keys=False, separators=(',', ': ')))


def convertRequestToRequestWithCpiSignature(private_key, cpi_id,
                                            cpi_name, request,
                                            jwt_algorithm='RS256'):
  """Converts a regular registration request to contain cpiSignatureData
       using the given JWT signature algorithm.

    Args:
      private_key: (string) valid PEM encoded string.
      cpi_id: (string) valid cpiId.
      cpi_name: (string) valid cpiName.
      request: individual CBSD registration request (which is a dictionary).
      jwt_algorithm: (string) algorithm to sign the JWT, defaults to 'RS256'.
    """
  cpi_signed_data = {}
  cpi_signed_data['fccId'] = request['fccId']
  cpi_signed_data['cbsdSerialNumber'] = request['cbsdSerialNumber']
  cpi_signed_data['installationParam'] = request['installationParam']
  del request['installationParam']
  cpi_signed_data['professionalInstallerData'] = {}
  if cpi_id:
    cpi_signed_data['professionalInstallerData']['cpiId'] = cpi_id
  cpi_signed_data['professionalInstallerData']['cpiName'] = cpi_name
  cpi_signed_data['professionalInstallerData'][
      'installCertificationTime'] = datetime.utcnow().strftime(
          '%Y-%m-%dT%H:%M:%SZ')
  compact_jwt_message = jwt.encode(
      cpi_signed_data, private_key, jwt_algorithm)
  jwt_message = six.ensure_text(compact_jwt_message).split('.')
  request['cpiSignatureData'] = {}
  request['cpiSignatureData']['protectedHeader'] = jwt_message[0]
  request['cpiSignatureData']['encodedCpiSignedData'] = jwt_message[1]
  request['cpiSignatureData']['digitalSignature'] = jwt_message[2]


def addIdsToRequests(ids, requests, id_field_name):
  """Adds CBSD IDs or Grant IDs to any given request.

  This function uses the following logic:
   - If the ID field is missing in request[i], it is filled with ids[i].
     - Note: len(ids) == len(requests) is required in this case.
   - If the ID field in request[i] is equal to 'REMOVE', the field is deleted.
   - If the ID field in request[i] is an integer k, it is replaced with ids[k].

  Args:
    ids: (list) a list of valid CBSD IDs or Grant IDs.
    requests: (list) list of requests, can contain empty dictionaries.
    id_field_name: (string) 'cbsdId' or 'grantId'.
  """
  for index, req in enumerate(requests):
    if id_field_name not in req:
      if len(ids) != len(requests):
        raise ValueError('Bad number of requests')
      req[id_field_name] = ids[index]
    elif req[id_field_name] == 'REMOVE':
      del req[id_field_name]
    elif isinstance(req[id_field_name], int):
      req[id_field_name] = ids[req[id_field_name]]
    # Else use the value that was provided in the config directly.


def addCbsdIdsToRequests(cbsd_ids, requests):
  """Adds CBSD IDs to a given request. See addIdsToRequests() for more info.

  Args:
    cbsd_ids: (list) list of CBSD IDs to be inserted into the request.
    requests: (list) list of requests, containing dictionaries.
  """
  addIdsToRequests(cbsd_ids, requests, 'cbsdId')


def addGrantIdsToRequests(grant_ids, requests):
  """Adds Grant IDs to a given request. See addIdsToRequests() for more info.

  Args:
    grant_ids: (list) list of Grant IDs to be inserted into the request.
    requests: (list) list of requests, containing dictionaries.
  """
  addIdsToRequests(grant_ids, requests, 'grantId')

def getCertificateFingerprint(certificate):
  """ Get SHA1 hash of the input certificate.
  Args:
    certificate: The full path to the file containing the certificate
  Returns:
    sha1 fingerprint of the input certificate
  """
  certificate_string = open(certificate,"rb").read()
  cert = load_certificate(FILETYPE_PEM, certificate_string)
  sha1_fingerprint = cert.digest("sha1")
  return six.ensure_text(sha1_fingerprint)

def localTime(utctime):
  """Convert UTC time to local time"""
  
  t = datetime.datetime.strptime(utctime, '%Y-%m-%dT%H:%M:%SZ')
  return pytz.utc.localize(t, is_dst=None).astimezone(local_tz)
